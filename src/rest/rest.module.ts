import { Module } from '@nestjs/common';
import { SoapModule, SoapModuleOptions } from 'nestjs-soap';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { RestController } from './rest.controller';
import { RestService } from './rest.service';

@Module({
  imports: [
    SoapModule.forRootAsync({
      clientName: 'SOAP_SERVER',
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (
        configService: ConfigService,
      ): Promise<SoapModuleOptions> => ({
        clientName: 'SOAP_SERVER',
        uri: configService.get<string>('SOAP_URI'),
        clientOptions: {
          // stopNodes: ["parse-me-as-string"],
          wsdl_options: {
            overrideRootElement: {
              // namespace: 'xmlns:ser',
              // xmlnsAttributes: [{
              //   name: 'xmlns:soapenv',
              //   value: "http://schemas.xmlsoap.org/soap/envelope/"
              // },
              // {
              //   name: 'xmlns:tem',
              //   value: "http://tempuri.org/"
              // }]
            },
          },
        },
        // auth: {
        //   username: configService.get<string>('soap.username'),
        //   password: configService.get<string>('soap.password'),
        // },
      }),
    }),
  ],
  controllers: [RestController],
  providers: [RestService],
})
export class RestModule {}
