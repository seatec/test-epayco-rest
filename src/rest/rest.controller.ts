import { Controller, Post, Req, Res, Body } from '@nestjs/common';
import { RestService } from './rest.service';
import { Response } from 'express';
import { ConsultaSaldoDto } from './dtos/consultaSaldoDto';
import { RegistroClientesDto } from './dtos/registerClientDto';
import { RecargaBilleteraDto } from './dtos/recargaBilleteraDto';
import { PagarDto } from './dtos/pagarDto';
import { ConfirmaPagoDto } from './dtos/confirmaPagoDto';

@Controller('rest')
export class RestController {
  constructor(private readonly restService: RestService) {}

  @Post('registroClientes')
  async registroClientes(
    @Body() registerClientDto: RegistroClientesDto,
    @Req() req,
    @Res() response: Response,
  ) {
    try {
      const [{ registroClientesResult }] =
        await this.restService.registerClientService(registerClientDto);
      const { success, message, data, code } = registroClientesResult;
      response.status(code).json({ success, message, data, code });
    } catch (e) {
      response.status(500).json({ status: false, message: e.message });
    }
  }
  @Post('consultaSaldo')
  async consultaSaldo(
    @Body() consultaSaldoDto: ConsultaSaldoDto,
    @Req() req,
    @Res() response: Response,
  ) {
    // const infoService = { ...soapWebServices[payload.service] };
    try {
      const [{ consultaSaldoResult }] =
        await this.restService.consultaSaldoService(consultaSaldoDto);

      const { success, message, data, code } = consultaSaldoResult;
      response.status(code).json({ success, message, data, code });
    } catch (e) {
      console.log(e.message);
      response.status(500).json({ status: false, message: e.message });
    }
  }

  @Post('recargaBilletera')
  async recargaBilletera(
    @Body() recargaBilleteraDto: RecargaBilleteraDto,
    @Req() req,
    @Res() response: Response,
  ) {
    // const infoService = { ...soapWebServices[payload.service] };
    try {
      const [{ recargaBilleteraResult }] =
        await this.restService.recargaBilleteraService(recargaBilleteraDto);
      const { success, message, data, code } = recargaBilleteraResult;
      response.status(code).json({ success, message, data, code });
    } catch (e) {
      response.status(500).json({ status: false, message: e.message });
    }
  }

  @Post('pagar')
  async pagar(
    @Body() pagarDto: PagarDto,
    @Req() req,
    @Res() response: Response,
  ) {
    // const infoService = { ...soapWebServices[payload.service] };
    try {
      const [{ pagarResult }] =
        await this.restService.pagarService(pagarDto);
      const { success, message, data, code } = pagarResult;
      response.status(code).json({ success, message, data, code });
    } catch (e) {
      response.status(500).json({ status: false, message: e.message });
    }
  }

  @Post('confirmaPago')
  async confirmaPago(
    @Body() confirmaPagoDto: ConfirmaPagoDto,
    @Req() req,
    @Res() response: Response,
  ) {
    // const infoService = { ...soapWebServices[payload.service] };
    try {
      const [{ confirmaPagoResult }] =
        await this.restService.confirmaPagoService(confirmaPagoDto);
      const { success, message, data, code } = confirmaPagoResult;
      response.status(code).json({ success, message, data, code });
    } catch (e) {
      response.status(500).json({ status: false, message: e.message });
    }
  }
}
