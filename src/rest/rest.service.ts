import { Inject, Injectable } from '@nestjs/common';
import { Client } from 'nestjs-soap';

@Injectable()
export class RestService {
  constructor(@Inject('SOAP_SERVER') private readonly soapClient: Client) {}

  async registerClientService(client) {
    return await this.soapClient.registroClientesAsync({
      client,
    });
  }

  async consultaSaldoService(filters) {
    return await this.soapClient.consultaSaldoAsync({
      filters,
    });
  }

  async recargaBilleteraService(recharge) {
    return await this.soapClient.recargaBilleteraAsync({
      recharge
    });
  }

  async pagarService(pay) {
    return await this.soapClient.pagarAsync({
      pay
    });
  }

  async confirmaPagoService(check) {
    return await this.soapClient.confirmaPagoAsync({
      check
    });
  }
}
