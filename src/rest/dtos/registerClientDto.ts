import { IsEmail, IsNotEmpty } from 'class-validator';

export class RegistroClientesDto {
  @IsNotEmpty()
  document: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  mobile_number: string;
}
