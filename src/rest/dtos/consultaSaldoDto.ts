import { IsNotEmpty } from 'class-validator';

export class ConsultaSaldoDto {
  @IsNotEmpty()
  document: string;

  @IsNotEmpty()
  mobile_number: string;
}
