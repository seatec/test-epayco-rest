import { IsNotEmpty } from 'class-validator';

export class PagarDto {
  @IsNotEmpty()
  document: string;

  @IsNotEmpty()
  mobile_number: string;

  @IsNotEmpty()
  amount: string;
}
