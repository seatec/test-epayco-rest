import { IsNotEmpty } from 'class-validator';

export class RecargaBilleteraDto {
  @IsNotEmpty()
  document: string;

  @IsNotEmpty()
  mobile_number: string;

  @IsNotEmpty()
  amount: string;
}
