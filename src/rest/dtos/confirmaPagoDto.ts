import { IsNotEmpty } from 'class-validator';

export class ConfirmaPagoDto {
  @IsNotEmpty()
  session_id: string;

  @IsNotEmpty()
  token: string;
}
